package Modelo;

public class Cotizacion {
    int numcot;
    String descripcion;
    float precio;
    float ppagoinicial;
    int plazo;
    
    public Cotizacion(){
       
    }
    public Cotizacion(int numcot, String descripcion, float precio, float ppagoinicial, int plazo){
        this.numcot = numcot;
        this.descripcion = descripcion;
        this.precio = precio;
        this.ppagoinicial = ppagoinicial;
        this.plazo = plazo;
    }
    
    public Cotizacion(Cotizacion cotizacion){
        this.numcot = cotizacion.numcot;
        this.descripcion = cotizacion.descripcion;
        this.precio = cotizacion.precio;
        this.ppagoinicial = cotizacion.ppagoinicial;
        this.plazo = cotizacion.plazo;
    }
    public void setNumCot(int nc){
        numcot = nc;
    }
    public int getNumCot(){
        return numcot;
    }
    public void setDescripcion(String desc){
        descripcion = desc;
    }
    public String getDescripcion(){
        return descripcion;
    }
    public void setPrecio(float pre){
        precio = pre;
    }
    public float getPrecio(){
        return precio;
    }
    public void setPPagoInicial(float pinicial){
        ppagoinicial = pinicial;
    }
    public float getPPagoInicial(){
        return ppagoinicial;
    }
    public void setPlazo(int plazos){
        plazo = plazos;
    }
    public int getPlazo(){
        return plazo;
    }
    public float calcularPagoInicial(){
        return getPrecio() * (getPPagoInicial()/100);
    }
    public float calcularTotalFin(){
        return getPrecio() - calcularPagoInicial();
    }
    public float calcularPagoMensual(){
        return calcularTotalFin()/getPlazo();
    }
}
