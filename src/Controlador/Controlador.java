package Controlador;
import Modelo.Cotizacion;
import Vista.dlgCotizacion;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.WindowConstants;

public class Controlador implements ActionListener{

    
    private Cotizacion cot;
    private dlgCotizacion vista;
    
    public Controlador(Cotizacion cot, dlgCotizacion vista){
        
        this.cot = cot;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        
    }
    
   private void iniciarVista(){
       vista.setTitle("Cotizador de Automoviles");
       vista.setSize(570,620);
       vista.setLocationRelativeTo(null);
       vista.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
       vista.setVisible(true);
       vista.dispose();
       System.exit(0);
   }
    
    
   @Override
   public void actionPerformed(ActionEvent e) {
       
       if (e .getSource() == vista.btnLimpiar){
           vista.txtDescripcion.setText("");
           vista.txtNumeroCotizacion.setText("");
           vista.txtPagoInicial.setText("");
           vista.txtPagoMensual.setText("");
           vista.txtPorcentajePagoInicial.setText("");
           vista.txtPrecio.setText("");
           vista.txtTotalFin.setText("");
           if (cot.getDescripcion() != null && cot.getNumCot() != 0 && cot.getPPagoInicial() !=0 && cot.getPrecio() != 0 ){
               vista.btnMostrar.setEnabled(true);
               vista.btnGuardar.setEnabled(true);
           } 
           else{
               vista.btnMostrar.setEnabled(false);
               vista.btnGuardar.setEnabled(false);
           }
        }
       
       
       if(e.getSource() == vista.btnNuevo){
           vista.txtDescripcion.setEnabled(true);
           vista.txtDescripcion.setBorder(BorderFactory.createLineBorder(Color.black));
           vista.txtNumeroCotizacion.setEnabled(true);
           vista.txtNumeroCotizacion.setBorder(BorderFactory.createLineBorder(Color.black));
           vista.txtPorcentajePagoInicial.setEnabled(true);
           vista.txtPorcentajePagoInicial.setBorder(BorderFactory.createLineBorder(Color.black));
           vista.txtPrecio.setEnabled(true);
           vista.txtPrecio.setBorder(BorderFactory.createLineBorder(Color.black));
           vista.comboPlazos.setEnabled(true);
           if (cot.getDescripcion() != null && cot.getNumCot() != 0 && cot.getPPagoInicial() !=0 && cot.getPrecio() != 0 ){
               vista.btnLimpiar.doClick(0);
           } 
           
       }
       
       if(e.getSource() == vista.btnCancelar){
           vista.txtDescripcion.setText("");
           vista.txtDescripcion.setEnabled(false);
           vista.txtDescripcion.setBorder(null);
           vista.txtNumeroCotizacion.setText("");
           vista.txtNumeroCotizacion.setEnabled(false);
           vista.txtNumeroCotizacion.setBorder(null);
           vista.txtPagoInicial.setText("");
           vista.txtPagoMensual.setText("");
           vista.txtPorcentajePagoInicial.setText("");
           vista.txtPorcentajePagoInicial.setEnabled(false);
           vista.txtPorcentajePagoInicial.setBorder(null);
           vista.txtPrecio.setText("");
           vista.txtPrecio.setEnabled(false);
           vista.txtPrecio.setBorder(null);
           vista.txtTotalFin.setText("");
           vista.comboPlazos.setEnabled(false);
           vista.btnGuardar.setEnabled(false);
           vista.btnMostrar.setEnabled(false);
       }
       
       if(e.getSource() == vista.btnCerrar){
           int result = JOptionPane.showConfirmDialog(vista, "¿Seguro que deseas salir?", "Advertencia", JOptionPane.YES_NO_OPTION);
           if (result == JOptionPane.YES_OPTION)
               System.exit(0);
           else if (result == JOptionPane.NO_OPTION)
               vista.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
       }
       
       if (e.getSource() == vista.btnGuardar){
           try{
                cot.setDescripcion(vista.txtDescripcion.getText());
                cot.setNumCot(Integer.valueOf(vista.txtNumeroCotizacion.getText()));
                cot.setPPagoInicial(Float.parseFloat(vista.txtPorcentajePagoInicial.getText()));
                cot.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                String plazoSeleccionado = vista.comboPlazos.getSelectedItem().toString();
                String primerosDosCaracteres = plazoSeleccionado.substring(0, 2);
                int plazoEntero = Integer.parseInt(primerosDosCaracteres);
                cot.setPlazo(plazoEntero);
                vista.txtPagoInicial.setText(Float.toString(cot.calcularPagoInicial()));
                vista.txtPagoInicial.setDisabledTextColor(Color.black);
                vista.txtPagoMensual.setText(Float.toString(cot.calcularPagoMensual()));
                vista.txtPagoMensual.setDisabledTextColor(Color.black);
                vista.txtTotalFin.setDisabledTextColor(Color.black);
                vista.txtTotalFin.setText(Float.toString(cot.calcularTotalFin()));
                JOptionPane.showMessageDialog(vista, "Guardado exitosamente");
           }catch(NumberFormatException ex){
               JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
           }catch(Exception ex2){
               JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage());
           }
       }
       
       if (e.getSource() == vista.btnMostrar){
           vista.txtDescripcion.setText(cot.getDescripcion());
           vista.txtNumeroCotizacion.setText(Integer.toString(cot.getNumCot()));
           vista.txtPagoInicial.setText(Float.toString(cot.calcularPagoInicial()));
           vista.txtPagoMensual.setText(Float.toString(cot.calcularPagoMensual()));
           vista.txtPorcentajePagoInicial.setText(Float.toString(cot.getPPagoInicial()));
           vista.txtPrecio.setText(Float.toString(cot.getPrecio()));
           vista.txtTotalFin.setText(Float.toString(cot.calcularTotalFin()));
       }
       
    }
    
    public static void main(String[] args) {
       
        Cotizacion cot = new Cotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(),true);
        
        Controlador control = new Controlador(cot,vista);
        control.iniciarVista();
    }
    
}
